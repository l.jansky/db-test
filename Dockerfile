FROM ljansky/alpine-mysql-liquibase:latest

COPY docker/liquibase.properties /tmp/liquibase.properties
COPY docker/changelog.xml /tmp/changelog.xml

COPY docker/db_fill.sh /usr/local/bin/db_fill.sh
RUN chmod 777 /usr/local/bin/db_fill.sh

COPY docker/db_start.sh /usr/local/bin/db_start.sh
RUN chmod 777 /usr/local/bin/db_start.sh

COPY docker/db_healthcheck.sh /usr/local/bin/db_healthcheck.sh
RUN chmod 777 /usr/local/bin/db_healthcheck.sh

HEALTHCHECK --interval=100ms --timeout=1s --start-period=3s CMD /usr/bin/mysql --user=root --execute "SHOW DATABASES;"

ENTRYPOINT [ "db_start.sh" ]