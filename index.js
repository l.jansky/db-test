const Docker = require('dockerode');
const docker = new Docker({socketPath: '/var/run/docker.sock'});
const EventEmitter = require('events');

const dockerEmitter = new EventEmitter();

const init = async (context, connectionInfo, forceBuild) => {
	const buildPromise = new Promise((resolve, reject) => {
		const image = docker.buildImage({
			context,
			src: ['Dockerfile', 'changelogs']
		}, {
			t: 'db-test',
			nocache: forceBuild,
			buildargs: {
				DB_NAME: connectionInfo.database,
				DB_USER: connectionInfo.user,
				DB_PASSWORD: ''
			}
		}, (error, output) => {
			if (error) {
				reject(error);
			}

			output.pipe(process.stdout);
			output.on('end', () => {
				resolve();
			});
		});
	});

	await buildPromise;

	docker.getEvents({
		filters: {
			type: {	container: true	},
			event: { health_status: true },
			image: { 'db-test': true }
		}
	}, function (err, data) {
	    if(err){
	        console.log(err.message);
	    } else {
	        data.on('data', function (chunk) {
	        	const event = JSON.parse(chunk.toString('utf8'));
	        	if (event.Type === 'container' && event.Action === 'health_status: healthy') {
	        		dockerEmitter.emit('healthy', event);
	        	}
	        });
	    } 
	});
};

const start = async () => {
	const container = await docker.createContainer({
		Image: 'db-test',
		HostConfig: {
			PortBindings: {'3306/tcp': [{ HostPort: '3306' }]}
		},
		Env: [
			'TIMEOUT=5'
		]
	});

	const mysqlReady = new Promise(resolve => {
		dockerEmitter.once('healthy', event => {
			resolve(event);
		});
	});

	await container.start();
	await mysqlReady;
	return container;
};

const remove = async (container) => {
	await container.stop({ t: 0 });
    await container.remove();
};

module.exports = {
	init,
	start,
	remove
};